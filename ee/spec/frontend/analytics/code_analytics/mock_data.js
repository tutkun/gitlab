export const group = {
  id: 1,
  name: 'foo',
  path: 'foo',
  avatar_url: 'host/images/group/image.svg',
};

export const project = {
  id: 1,
  name: 'bar',
  path: 'bar',
  avatar_url: 'host/images/project/image.svg',
};

export const DEFAULT_FILE_QUANTITY = 100;
